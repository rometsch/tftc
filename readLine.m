function readLine(filename,t_min,t_max)
    % This function loads an image and lets the user pick a line on the
    % image. On this line rgb values are read and translated to xyz
    % coordinate and finally to a corresponding thickness using tftc.
    tcobj = tftc;
    tcobj.calc_tct(2000,t_min,t_max);
    
    fig = figure;
    hold on
    himage = imshow(filename);
    profile_button = uicontrol('Parent',fig,'Style','pushbutton','Position',[45,6,70,23],...
                'String','get thickness','Callback',  @calc_thickness);
    
    pixel_button = uicontrol('Parent',fig,'Style','pushbutton','Position',[150,6,70,23],...
                'String','pixel thickness','Callback',  @calc_pixel_thickness);
            
    h = imdistline();
            
    function calc_pixel_thickness(src,event)
        [x,y] = getpts(fig);
        rgb = get_pixel_rgb(x,y);
        xyz = tftc.rgb2chrom(rgb);
        [ind1, ind2] = tcobj.find_closest(xyz);
        xyz_on_curve = tcobj.get_tct_xyz(ind1);
        fig2 = figure();
        hold on
        tftc.plot_adobe_RGB_triangle();
        plot(xyz(1),xyz(2),'Marker','*','Color',rgb);
        plot(xyz_on_curve(1),xyz_on_curve(2),'Color','black','Marker','d');
        if ind2 ~= 0
            xyz_on_curve_2 = tcobj.get_tct_xyz(ind2);
            plot(xyz_on_curve_2(1),xyz_on_curve_2(2),'Color','black','Marker','x');
        end
        tcobj.plot_xyz_trajectory_black();
        hold off
        axis([0,0.75,0,0.75]);
        tcobj.get_tct_t(ind1)
        tcobj.get_tct_t(ind2)
        
%         tftc.plot_rgb_in_xyz(rgb)
    end
    
    function rgb = get_pixel_rgb(x,y)
        x = round(x);
        y = round(y);
        rgb = [0.0 0.0 0.0];
        rgb(1) = himage.CData(y,x,1);
        rgb(2) = himage.CData(y,x,2);
        rgb(3) = himage.CData(y,x,3);
        rgb = rgb /255;
    end

    function calc_thickness(src,event)
        t = [];
        d = [];
        height = [];
        xyz_log = [];
        pos = h.getPosition();
        v1 = [pos(1,1) pos(1,2)];
        v2 = [pos(2,1) pos(2,2)];
        dist = h.getDistance();
        
        fig3 = figure();
        
        hold on
        
        N=500;
        for i=0:1:N
            v = v1+i/N*(v2-v1);
            rgb = get_pixel_rgb(v(1),v(2));
            xyz = tftc.rgb2chrom(rgb);
            for j=1:1:3
                xyz_log(i+1,j) = xyz(j);
            end
            [ind1, ind2] = tcobj.find_closest(xyz);
            t(i+1) = tcobj.get_tct_t(ind1);
            d(i+1) = i/N;
            height(i+1) = v(2);
%             plot(d(i+1),t(i+1),'Marker','o','Color',rgb);
%             if ind2 ~= 0
%                 t2 = tcobj.get_tct_t(ind2);
%                 plot(height(i+1),t2,'Color','black','Marker','x');
%             end
        end
        hold off
        plot(d,t,'Marker','*','LineStyle','none');
        xlabel('fraction of distance traveled on line');
%         xlabel('height in image in pixel, lower -> top, higher -> bottom');
        ylabel('thickness in nm');
        
        
        fig4 = figure();
        hold on
        tcobj.plot_xyz_trajectory_black();
        tftc.plot_adobe_RGB_triangle();
        scatter(xyz_log(:,1),xyz_log(:,2),'Marker','*');
        hold off
        
        
    end
end