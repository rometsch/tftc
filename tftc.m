classdef tftc < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        % cmf functions from http://cvrl.ioo.ucl.ac.uk/cmfs.htm, 2-deg XYZ CMFs transformed from the CIE (2006) 2-deg LMS cone fundamentals
        % lambda, r, g, b
        cmf = [390 0.00377 0.000415 0.0185
        395 0.00938 0.00106 0.0461 
        400 0.0221 0.00245 0.11    
        405 0.0474 0.00497 0.237   
        410 0.0895 0.00908 0.451   
        415 0.145 0.0143 0.738     
        420 0.204 0.0203 1.05      
        425 0.249 0.0261 1.31      
        430 0.292 0.0332 1.55      
        435 0.323 0.0416 1.75      
        440 0.348 0.0503 1.92      
        445 0.342 0.0574 1.92      
        450 0.322 0.0647 1.85      
        455 0.283 0.0724 1.66      
        460 0.249 0.0851 1.52      
        465 0.222 0.106 1.43       
        470 0.181 0.13 1.25        
        475 0.129 0.154 0.999      
        480 0.0818 0.179 0.755     
        485 0.046 0.206 0.562      
        490 0.0208 0.238 0.41      
        495 0.0071 0.285 0.311     
        500 0.00246 0.348 0.238    
        505 0.00365 0.428 0.172    
        510 0.0156 0.52 0.118      
        515 0.0432 0.621 0.0828    
        520 0.0796 0.718 0.0565    
        525 0.127 0.795 0.0375     
        530 0.182 0.858 0.0244     
        535 0.241 0.907 0.0157     
        540 0.31 0.954 0.00985     
        545 0.38 0.981 0.00613     
        550 0.449 0.989 0.00379    
        555 0.528 0.999 0.00233    
        560 0.613 0.997 0.00143    
        565 0.702 0.99 0.000882    
        570 0.797 0.973 0.000545   
        575 0.885 0.942 0.000339   
        580 0.964 0.896 0.000212   
        585 1.05 0.859 0.000134    
        590 1.11 0.812 8.49e-05    
        595 1.14 0.754 5.46e-05    
        600 1.15 0.692 3.55e-05    
        605 1.13 0.627 2.33e-05    
        610 1.08 0.558 1.55e-05    
        615 1.01 0.49 1.05e-05     
        620 0.914 0.423 0          
        625 0.814 0.361 0          
        630 0.692 0.298 0          
        635 0.576 0.242 0          
        640 0.473 0.194 0          
        645 0.384 0.155 0          
        650 0.3 0.119 0            
        655 0.228 0.0898 0         
        660 0.171 0.0667 0         
        665 0.126 0.049 0          
        670 0.0922 0.0356 0        
        675 0.0664 0.0255 0        
        680 0.0471 0.0181 0        
        685 0.0329 0.0126 0        
        690 0.0226 0.00866 0       
        695 0.0158 0.00603 0       
        700 0.011 0.0042 0         
        705 0.00761 0.00291 0      
        710 0.00521 0.002 0        
        715 0.00357 0.00137 0      
        720 0.00246 0.000945 0     
        725 0.0017 0.000654 0      
        730 0.00119 0.000456 0     
        735 0.000827 0.000318 0    
        740 0.000576 0.000222 0    
        745 0.000406 0.000157 0    
        750 0.000286 0.00011 0     
        755 0.000202 7.83e-05 0    
        760 0.000144 5.58e-05 0    
        765 0.000102 3.98e-05 0    
        770 7.35e-05 2.86e-05 0    
        775 5.26e-05 2.05e-05 0    
        780 3.81e-05 1.49e-05 0    
        785 2.76e-05 1.08e-05 0    
        790 2e-05 7.86e-06 0       
        795 1.46e-05 5.74e-06 0    
        800 1.07e-05 4.21e-06 0    
        805 7.86e-06 3.11e-06 0    
        810 5.77e-06 2.29e-06 0    
        815 4.26e-06 1.69e-06 0    
        820 3.17e-06 1.26e-06 0    
        825 2.36e-06 9.42e-07 0    
        830 1.76e-06 7.05e-07 0];
    end
    
    properties
        % refractive index of soap solution
        mu = 1.333;
        % refractive angle
        theta = 0;
        % thickness of film
        t = 0;
        % minimum wavelength of visibble light in nm
        lambdaMin = tftc.cmf(1,1);
        % maximum wavelength of visibble light in nm
        lambdaMax = tftc.cmf(end,1);
        % linearly spaced vector for lambda sample points
        lambda = transpose(tftc.cmf(:,1));
        % refelctance
        R = [];
        % spectral power distribution of source
        S = [];
        % rgb values for current reflectance spectrum
        rgb = [];
        % cromaticity values
        xyz =[];
        % color thickness table
        % thickness in nm, x, y, z
        tct = [];
    end
    
    methods
        function obj = tftc(mu,theta)
            % constructor
            if nargin>0
                obj.mu=mu;
                obj.theta=theta;
            end
%             obj.S = ones(1,length(obj.lambda));
            obj.calc_S();
        end
        
        function obj = calc_S(obj)
            % This function calculates aa blackbody spectrum for T=3200 K
            % and stores the distribution in S. The spectrum is normalized
            % to its highest value which, for the visible range, is at the
            % right boundary
            for i=1:1:length(obj.lambda)
                l = obj.lambda(i);
                obj.S(i) = 1/l^5/(exp(4486/l)-1);
            end
            % Normalize S
            obj.S = obj.S/obj.S(end);
        end
        
        function obj = calc_R(obj)
            % for every sample point in lambda calculate the relative
            % intensity with relR and store it in distInten
            obj.R = tftc.refelctance(obj.lambda,obj.t,obj.mu,obj.theta);
        end
        
        function obj = plot_R(obj)
            % compute R
            obj.calc_R();
            % compute rgb
            obj.calc_xyz();
            obj.calc_rgb();
            % plot the intensity distribution
            plot(obj.lambda,obj.R);
            title(['Reflectance curve `for thin film with t = ',num2str(obj.t),'nm.']);
            ylabel('relectance curve');
            xlabel('wavelength in nm');
            axis([obj.lambdaMin,obj.lambdaMax,0,1]);
            set(gca,'XAxisLocation','top','YAxisLocation','left');
            annotation('textbox',[0.2 0.7 0.2 0.025],'BackgroundColor','white','String',num2str(obj.rgb));
            annotation('rectangle',[0.2 0.5 0.2 0.2],'FaceColor',obj.rgb);
        end
        
        function obj = interact_spectrum(obj)
            
            % Build the GUI
            init_ui;

            % This funciton sets up the figure and slider.
            % note the CreateFcn and the Callback.
            % The CreatFcn runs the first time while the Callback
            % runs each time you move the slider.
            function init_ui()
                fig = figure('Position',[450 450 800 600]);
                tMin = 10;
                tMax = 400;
                labelMin = uicontrol('Parent',fig,'Style','text','Position',[115,6,30,23],...
                'String',num2str(tMin));
                labelMax = uicontrol('Parent',fig,'Style','text','Position',[505,6,30,23],...
                'String',num2str(tMax));
                labelName = uicontrol('Parent',fig,'Style','text','Position',[45,6,55,23],...
                'String','Thickness t');
                slider1 = uicontrol('Style', 'slider',...
                                    'Min',tMin,'Max',tMax,'Value',50,...
                                    'Position', [150 10 350 20],...
                                    'CreateFcn', @calc_and_plot,...
                                    'Callback',  @calc_and_plot);
            end

            % This funciton it what is run when you first run the 
            % code and each time you move the slider.
            function calc_and_plot(src,event)

                % Get the current slider value
                obj.t = get(src,'Value');
                
                % Update Plot
                obj.plot_R()
            end

        end
        
        function obj = interact_xyz(obj)
            % This function simply plots the xy coordinate for adjustable
            % thickness
            
            % Build the GUI
            init_ui;

            % This funciton sets up the figure and slider.
            % note the CreateFcn and the Callback.
            % The CreatFcn runs the first time while the Callback
            % runs each time you move the slider.
            function init_ui()
                fig = figure('Position',[450 450 800 600]);
                tMin = 10;
                tMax = 500;
                labelMin = uicontrol('Parent',fig,'Style','text','Position',[115,6,30,23],...
                'String',num2str(tMin));
                labelMax = uicontrol('Parent',fig,'Style','text','Position',[505,6,30,23],...
                'String',num2str(tMax));
                labelName = uicontrol('Parent',fig,'Style','text','Position',[45,6,55,23],...
                'String','Thickness t');
                slider1 = uicontrol('Style', 'slider',...
                                    'Min',tMin,'Max',tMax,'Value',200,...
                                    'Position', [150 10 350 20],...
                                    'CreateFcn', @calc_and_plot,...
                                    'Callback',  @calc_and_plot);
            end

            % This funciton it what is run when you first run the 
            % code and each time you move the slider.
            function calc_and_plot(src,event)

                % Get the current slider value
                obj.t = get(src,'Value');
                
                % Caclculate new relectivity
                obj.calc_R();
                
                % Calculate cromaticity values
                obj.calc_xyz();
                
                % Clear current figure
                cla(gca);
                
                % Update Plot
                hold on
%                 obj.plot_xyz_trajectory();
                % Plot the adobe RGB triangle
                tftc.plot_adobe_RGB_triangle();
                
                % Plot new coordinates
                obj.plot_xyz();
                hold off
            end
        end
        
        function obj = plot_xyz(obj)
            % This is a function to plot the xy coordinate
            % Plot the coordinate as a marker
            obj.calc_xyz();
            obj.calc_rgb();
%             plot(obj.xyz(1),obj.xyz(2),'Marker','o','Color',obj.rgb);
            if min(obj.rgb) < 0
                plot(obj.xyz(1),obj.xyz(2),'Marker','*','Color',[0 0 0]);
            else
                plot(obj.xyz(1),obj.xyz(2),'Marker','*','Color',obj.rgb);
            end
            title(['Cromaticity coordinate for thickness t = ',num2str(obj.t),'nm.']);
            xlabel('x');
            ylabel('y');
            axis([0,0.75,0,0.75]);
            annotation('textBox','String',['x=',num2str(obj.xyz(1)),'  ','y=',num2str(obj.xyz(2))],'Position',[0.2 0.8 0.2 0.03],'BackgroundColor','w');
        end
        
        function obj = calc_tct(obj,N_arg,t_min_arg,t_max_arg)
            % Calculate a table containing xyz values for a range of
            % thickness values
            
            % Number of thickness values
            if nargin == 2
                N = N_arg;
            elseif nargin == 4
                N = N_arg;
                t_min = t_min_arg;
                t_max = t_max_arg;
            else
                N = 2000;
                t_min = 10;
                t_max = 1000;
            end
            
            % Initialize new tct
            obj.tct = [];
            
            % Initialize thickness vector
            obj.tct(:,1) = linspace(t_min,t_max,N);
            
            % Calculate xyz for every thickness
            for i = 1:1:N
                obj.t = obj.tct(i,1);
                obj.calc_R();
                obj.calc_xyz();
                obj.tct(i,2) = obj.xyz(1);
                obj.tct(i,3) = obj.xyz(2);
                obj.tct(i,4) = obj.xyz(3);
            end
            
        end
        
        function obj = plot_xyz_trajectory(obj)
            % Plot a trajectory in cromaticity space for a range of
            % thickness
            % Compute thickness color table
            N = 2000;
            t_min = 10;
            t_max = 1000;
            obj.calc_tct(N,t_min,t_max);
            
            hold on
            
            % Plot adobe RGB boundaries
            tftc.plot_adobe_RGB_triangle()
            
            
            for i = 1:1:N
                obj.xyz(1) = obj.tct(i,2);
                obj.xyz(2) = obj.tct(i,3);
                obj.xyz(3) = obj.tct(i,4);
                obj.calc_rgb();
                if min(obj.rgb) < 0
                    plot(obj.tct(i,2),obj.tct(i,3),'Marker','.','Color',[0 0 0]);
                else
                    plot(obj.tct(i,2),obj.tct(i,3),'Marker','.','Color',obj.rgb);
                end
            end
            
            hold off
            
            title(['Cromaticity curve for a range of thickness from ',num2str(t_min),' to ', num2str(t_max),'nm.']);
            xlabel('x');
            ylabel('y');
            axis([0,0.75,0,0.75]);
        end
        
        function obj = plot_xyz_trajectory_black(obj)
            plot(obj.tct(:,2),obj.tct(:,3),'Color','black');
        end
            
        function obj = calc_xyz(obj)
            % This function calculates the cromaticity values for the given
            % spectral power distribution S and the spectral reflectance
            % factor R
            
            obj.xyz = obj.spectrum2xyz(obj.S.*obj.R);
        end
        
        function obj = calc_rgb(obj)
            % This calls the
            % appropriate function to convert the xyz values to rgb
            % values using a built in MATLAB function
            obj.rgb = xyz2rgb(obj.xyz,'ColorSpace','adobe-rgb-1998'); %'sRGB');
        end
        
        function obj = view_color(obj)
            % This function calculates the rgb value and generates a figure
            % to display the color
%             obj.calc_xyz();
%             obj.calc_rgb();
            scrsz = get(groot,'ScreenSize');
%             figure('Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2])
            fig = figure('Color',obj.rgb,'Position',[1 scrsz(4)-scrsz(4)/6 scrsz(3)/6 scrsz(4)/6]);
%             labelName = uicontrol('Parent',fig,'Style','text','Position',[45,6,55,23],...
%                 'String','','BackgroundColor',obj.rgb);
        end
        
        function [ind1, ind2] = find_closest(obj,xyz)
            % Find closest matching point on curve
            % Jumpstart minimizer
            xyz_aux = [obj.tct(1,2) obj.tct(1,3) obj.tct(1,4)];
            dsq = (xyz_aux(1)-xyz(1))^2+(xyz_aux(2)-xyz(2))^2;
            ind1 = 1;
            for i=2:1:length(obj.tct(:,1))
                xyz_aux = [obj.tct(i,2), obj.tct(i,3), obj.tct(i,4)];
                dsq_new = (xyz_aux(1)-xyz(1))^2+(xyz_aux(2)-xyz(2))^2;
                if dsq_new < dsq
                    dsq = dsq_new;
                    ind1 = i;
                end
            end
            
            % Find second match in opposite direction, if available
            
%             xyz_min = obj.get_tct_xyz(ind1);
%             v0 = xyz_min - xyz;
%             v0 = v0/sqrt(v0*transpose(v0));
%             % Jumpstart search
%             ind2 = 0;
%             dsq = 1000;
%             
%             figure
%             hold on
%             
%             for i=1:1:length(obj.tct(:,1))
%                 % Calculate angle between vectors
%                 xyz_aux = [obj.tct(i,2), obj.tct(i,3), obj.tct(i,4)];
%                 v = xyz_aux - xyz;
%                 v = v/sqrt(v*transpose(v));
%                 beta = acos(v*transpose(v0));
%                 if beta > pi-pi/4
%                     plot(xyz_aux(1),xyz_aux(2),'Marker','*','Color','black');
%                    dsq_new = (xyz_aux(1)-xyz(1))^2+(xyz_aux(2)-xyz(2))^2;
%                     if dsq_new < dsq
%                         dsq = dsq_new;
%                         ind2 = i;
%                     end 
%                 end
%             end
% 
%             hold off

            % Find second match with an inde at least 200 away
            % Jumpstart minimizer
            xyz_aux = [obj.tct(1,2) obj.tct(1,3) obj.tct(1,4)];
            dsq = (xyz_aux(1)-xyz(1))^2+(xyz_aux(2)-xyz(2))^2;
            ind2 = 1;
            for i=1:1:length(obj.tct(:,1))
                xyz_aux = [obj.tct(i,2), obj.tct(i,3), obj.tct(i,4)];
                dsq_new = (xyz_aux(1)-xyz(1))^2+(xyz_aux(2)-xyz(2))^2;
                if abs(i - ind1) > 500
                    if dsq_new < dsq
                        dsq = dsq_new;
                        ind2 = i;
                    end
                end
            end
        end
        
        function t = get_tct_t(obj,index)
            t = obj.tct(index,1);
        end
        
        function xyz = get_tct_xyz(obj,index)
            for i=1:1:3
                xyz(i) = obj.tct(index,i+1);
            end
        end
            
    end
    
    
    methods (Static)
        function res = refelctance(lambda,t,mu,theta) 
            % Calculate the intensity relative to the maximal intensity.
            % Formula taken from Isenberg 1978, the science of soap bubbles and
            % soap films, eq 1.28.
            % @param:
            %   lambda:     wavelength in nm
            %   t:          thickness of the film in nm
            %   mu:         refractive index of the film
            %               outside the film mu=1 assumed
            %   theta:      angle of refraction (default=0)
            
            % test whether lambda is positive
            if lambda < 0
                error('refelctance:lambdaMustBePositive','Wavelength must be positive!');
            end
            res = sin(2*pi*mu*t*cos(theta)./lambda).^2;
        end
        
        function xyz = spectrum2xyz(P)
            % This function calculates the cromaticity values for the given
            % spectral power distribution S and the spectral reflectance
            % factor R
            
            % get the color matching functions
            x_cmf = transpose(tftc.cmf(:,2));
            y_cmf = transpose(tftc.cmf(:,3));
            z_cmf = transpose(tftc.cmf(:,4));
            
            % Calculate tristimulus values
            X = sum(P.*x_cmf);
            Y = sum(P.*y_cmf);
            Z = sum(P.*z_cmf);
            
            % Calculate cromaticities.
            xyz(1) = X./(X+Y+Z);
            xyz(2) = Y./(X+Y+Z);
            xyz(3) = Z./(X+Y+Z); 
        end
        
        function plot_adobe_RGB_triangle()
           % This function plots the boundaries of the adobe RGB space
           % values from http://en.wikipedia.org/wiki/Adobe_RGB_color_space
           plot([0.6400 0.2100 0.1500 0.6400],[0.3300 0.7100 0.0600 0.3300],'Color',[0 0 0]);
        end
        
        function plot_rgb_in_xyz(rgb)
           % This function plots the rgb value given in the argument in the
           % CIE color space
           hold on
           
           % Plot adobe RGB triangle
           tftc.plot_adobe_RGB_triangle();
           
           % Convert the rgb value to chromaticity coordinates using the
           % built in MATLAB function.
           xyz = rgb2xyz(rgb,'ColorSpace','adobe-rgb-1998')
           
           % Plot the coordinate
           plot(xyz(1),xyz(2),'Marker','.','Color',rgb);
           axis([0,0.75,0,0.75]);
        end
        
        function xyz = rgb2chrom(rgb)
            % Convert rgb to CIE
            XYZ = rgb2xyz(rgb,'ColorSpace','adobe-rgb-1998');
            % Convert CIE to chromaticity
            xyz = zeros(1,3,'double');
            for i=1:1:3
                xyz(i) = XYZ(i)/sum(XYZ);
            end 
        end
        
    end
    
    
end

