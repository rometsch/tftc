classdef Tests < matlab.unittest.TestCase
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        testInstance
    end
    
    methods(TestMethodSetup)
        function createInstance(testCase)
            testCase.testInstance = tftc;
        end
    end
 
    methods(TestMethodTeardown)
        function deleteInstance(testCase)
%             delete(testCase.testInstance)
        end
    end
    
    methods (Test)
        
        function testRefelctanceKnownOutput(testCase)
            fcnRes = testCase.testInstance.refelctance(521,200,1.333,0);
            actRes = sin(2*pi*1.333*200*cos(0)/521)^2;
            testCase.verifyEqual(fcnRes,actRes);
        end
        
        function testRefelctanceNegativeLambda(testCase)
            testCase.verifyError(@()testCase.testInstance.refelctance(-5,200,1,0),'refelctance:lambdaMustBePositive')
        end
        
        function testCreatelambda(testCase)
            testCase.testInstance.lambdaMin=100;
            testCase.testInstance.lambdaMax=500;
            testCase.testInstance.createLambdaRange(401);
            fcnRes = testCase.testInstance.lambda;
            actRes = linspace(100,500,401);
            testCase.verifyEqual(length(fcnRes),length(actRes));
            testCase.verifyEqual(fcnRes,actRes);
        end
        
        function testcalcR(testCase)
            % choose a thickness
            t = 200;
            % create an intensity distribution
            testCase.testInstance.calcR(t);
            fcnRes = testCase.testInstance.R;
            % get the sample values of lambda and parameters
            lambda = testCase.testInstance.lambda;
            mu = testCase.testInstance.mu;
            theta = testCase.testInstance.theta;
            % calculate the values by hand and store them
            actRes = [];
            for i = 1:length(lambda)
                actRes(i) = testCase.testInstance.refelctance(lambda(i),t,mu,theta);
            end
            % compare
            testCase.verifyEqual(length(fcnRes),length(actRes));
            testCase.verifyEqual(fcnRes,actRes);
        end
    end
    
end

